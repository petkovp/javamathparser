/*
 * Solves a mathematical expression using stack and decque 
 *
 */
package com.petkov.mathparser;

import com.petkov.exceptions.DivisionByZero;
import com.petkov.exceptions.InvalidInputQueueString;
import com.petkov.exceptions.NonBinaryExpression;
import com.petkov.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author Petko 
 * convertInfixToPostfix
 */
public class ResultParserWithParenthesis {

    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {

        Queue<String> output = new ArrayDeque();
        Stack<String> stack = new Stack<>();
        int openingParenthesisCount = 0;
        int closingParenthesisCount = 0;
        int completedParenthesis = 0;
        hasEmptyParentheses(infixQueue);
        detectedNonBinary(infixQueue);
        Queue<String> adaptedQueue = new ArrayDeque();
        adaptedQueue = transformQueue(infixQueue);

        
        for (String string : adaptedQueue) {

           
            if (!isNumeric(string) && !isSign(string)) {
                throw new InvalidInputQueueString(" invalid input: " + string);

            } else {
                
               
                if (isNumeric(string)) {
                    output.add(string);
                    
                } else if (string.equals("(")) {
                    
                    openingParenthesisCount++;
                    stack.push(string);
                } else if (string.equals(")")) {
                    
                    closingParenthesisCount++;
                    completedParenthesis++;
                     

                    while (!stack.isEmpty() && !stack.peek().equals("(")) {
                        output.add(stack.pop());

                    }
                    if (stack.peek().equals("(")) {
                        stack.pop();
                        
                    } else {
                        throw new NonMatchingParenthesis("Missing opening parenthesis");
                    }

                } else {
                    while (!stack.isEmpty() && getPriority(string) <= getPriority(stack.peek())) {
                        output.add(stack.pop());

                    }
                    stack.push(string);
                }

            }

        }
        while (!stack.isEmpty()) {
            if (stack.peek().equals("(")) {
                throw new NonMatchingParenthesis("too many parentesis");
            }
            output.add(stack.pop());
        }

        if(openingParenthesisCount!=closingParenthesisCount){
            throw new NonMatchingParenthesis("non matching parenthisis");
        }
 
        
        return output;
    }
    
    /**
     * returns a new arraydeque with modified parenthesis
     * @param queue
     * @return 
     */
    private Queue<String> transformQueue(Queue<String> queue){
       Deque<String> transformedQueue = new ArrayDeque<>();
       
      
        for(String s: queue){
             try{
            if(s.equals("-(")){
                transformedQueue.add("-1");
                transformedQueue.add("*");
                transformedQueue.add("(");
            }
            else if(s.equals(")-")){
                transformedQueue.add(")");
                transformedQueue.add("*");
                transformedQueue.add("-1");
            }
            else if(s.equals("+(")){
                //transformedQueue.add("+");
                transformedQueue.add("(");
            }
            else if(s.equals(")+")){
                transformedQueue.add(")");
                transformedQueue.add("+");
            }
            else if(s.equals("(") && isNumeric(transformedQueue.peekLast())){
                transformedQueue.add("*");
                transformedQueue.add("(");
            }
            else if(isNumeric(s)&& transformedQueue.peekLast().equals(")") ){
                transformedQueue.add("*");
                transformedQueue.add(s);
            }
            else if (s.equals("(") && transformedQueue.peekLast().equals(")") ){
                transformedQueue.add("*");
                transformedQueue.add("(");
                
            }
            else{
                transformedQueue.add(s);
            }}
            catch (NullPointerException e){
                System.out.print("null found with"+s);
                transformedQueue.add(s);
            }
                
        }
        
        
        return transformedQueue;
        
    }

    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero, NonBinaryExpression {

        System.out.print("POSTFIX: ");
        for(String element: postfixQueue){
            System.out.print(element+" ");
        } 
        
        Stack<Double> stack = new Stack<>();

        for (String string : postfixQueue) {
            
            if(string.equals(" ")){
                
            }
            else if (isNumeric(string) ) {
                
               
                stack.push(Double.parseDouble(string));
            } else {
                
              
                Double val1 = stack.pop();
                
                Double val2 = stack.pop();
                System.out.println("val2:: "+ val2 + " val1:: " + val1);

                switch (string) {
                    case "+":
                        System.out.println(val2 + " + " + val1);
                        stack.push(val2 + val1);
                        break;
                    case "-":
                        System.out.println(val2 + " + " + val1);
                        stack.push(val2 - val1);
                        break;
                    case "/":
                        if (val1 == 0 || val2 == 0) {
                            throw new DivisionByZero("division by zero detected");

                        } else {
                            System.out.println(val2 + " / " + val1);
                            stack.push(val2 / val1);
                        }
                        break;
                    case "*":
                        System.out.println(val2 + " * " + val1);
                        stack.push(val2 * val1);
                        break;
                    default:
                        stack.push(val2 + val1);
                        stack.push(Double.parseDouble(string));
                        break;

                }

            }
        }
        return stack.pop().toString();
    }
    
    /**
    * checks is a parentheses is missing
    **/
    private void hasEmptyParentheses(Queue<String> queue) throws NonBinaryExpression{
        String previousChar = "";
        for(String s: queue){
            if(s.equals(")") && previousChar.equals("(")){
                throw new NonBinaryExpression("Empty parentheses found");
            }
            previousChar=s;
        }
    }
    /**
    *returns the  order of precedence
    **/
    private int getPriority(String value) {

        switch (value) {

            case "*":
                return 2;
            case "/":
                return 2;
            case "+":
                return 1;
            case "-":
                return 1;

        }
        return -1;

    }
    /**
    *returns true if no invalid characters
    *throws InvalidInputQueueString if input contains invalid characters
    **/
    private boolean validateInput(String input) throws InvalidInputQueueString {
        String regex = "^(\\d+[\\+\\-\\*\\/]{1})+\\d+$";

        if (input.matches(regex)) {
            return true;
        } else {
            throw new InvalidInputQueueString(" invalid input");
        }

    }

    public boolean isNumeric(String str) {
        if(str == null){
            return false;
        }
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    public boolean isSign(String input) {
        String regex = "[\\)\\(\\+\\-\\*\\/]{1}";

        if (input.matches(regex)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isUnallowedConsecutiveSign(String input) {
        String regex = "[\\+\\-\\*\\/]{1}";

        if (input.matches(regex)) {
            return true;
        } else {
            return false;
        }
    }

//    public static void main(String[] args)  {
//        System.out.println(isSign("+"));
//
//    }

    
    private void detectedNonBinary(Queue<String> infixQueue) throws NonBinaryExpression {
       String previousString = "";
       
        for(String s : infixQueue){
            if(isNumeric(previousString) && isNumeric(s)){
                throw new NonBinaryExpression("two consecutive numbers");
                
            }
            
            else if(isUnallowedConsecutiveSign(previousString) && isUnallowedConsecutiveSign(s)){
                throw new NonBinaryExpression("two consecutive signs");
            }
            else{
                previousString = s;
            }
            
        }
        if(isUnallowedConsecutiveSign(previousString)){
            
            throw new NonBinaryExpression("cannot finish with a sign at the end");
        }
    }

    
}
